<?php namespace App\Http\Models\Frontend;

use App\Leancloud\AVQuery;
use App\Leancloud\AVObject;

class DashboardModel {
	//获取订单列表信息
	public function orders () {
        $query = new AVQuery('Order');
        $orders = $query->find();
        return $orders;
	}

	//查找指定条件的订单集
	public function findOrders ($conditions) {
		$query = new AVQuery('Order');
		$sharingType = $conditions['sharingType'];
		$startCity = $conditions['city'];
		$startPosition = $conditions['startPosition'];
		$endCity = $conditions['endCity'];
		$endPosition = $conditions['endPosition'];
		$startDate = $conditions['startDate'];
		if ($sharingType) {
			$query->whereEqualTo('sharingType', $sharingType);
		}
		if ($startCity) {
			$query->whereEqualTo('startCity', $startCity);
		}
		if ($startPosition) {
			$query->whereEqualTo('startPosition', $startPosition);
		}
		if ($endCity) {
			$query->whereEqualTo('endCity', $endCity);
		}
		if ($endPosition) {
			$query->whereEqualTo('endPosition', $endPosition);
		}
		if ($startDate) {
			$query->whereEqualTo('startDate', $startDate);
		}
		$orders = $query->find();
		return $orders;
	}
}