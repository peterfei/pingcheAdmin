@extends('frontend.base')
@section('content')
<div class="header-cont">
	<div class="header">
		<img class="logo" src="{{ asset('/frontend/images/dashboard/logo.png') }}">
		<div class="header_nav">
			<a class="sina">微博登录</a>
			<a class="qq">QQ登录</a>
            <a class="login">登录 | 免费注册 |</a>
            <a>帮助中心</a>
            <img src="{{ asset('/frontend/images/dashboard/icon2.png') }}">
            <a class="photo">手机版</a>
		</div>
	</div>
</div>
<div class="nav_cont">
    <ul class="nav">
        <li><a class="selected" href="#">首页</a></li>
        <li><a href="#">上下班拼车</a></li>
        <li><a href="#">长途拼车</a></li>
        <li><a href="#">即时拼车</a></li>
        <li><a href="#">私家车出租</a></li>
      <li><a href="#">找代驾</a></li>
        <div class="right_nav">
 		<p>发布路线</p>
    	</div>
    </ul>
</div>
<div class="banner">
	<div class="select_cont">
		<ul class="title">
			<li class="selected">上下班拼车</li>
			<li>长途拼车</li>
			<li>即时拼车</li>
			<li>找代驾</li>
		</ul>
		<div class="input_cont">
			<div>
			<label>城市：</label><input class="city" type="text" value="北京">
			</div>
			<div>
			<label>起点：</label><input type="text" value="小区/地标/写字楼 等">
			</div>
			<div>
			<label>终点：</label><input type="text" value="小区/地标/写字楼 等">
			</div>
			<div>
			<label>出发时间：</label><input type="text" value="请选择">
			</div>
            <div class="input_btn">
            	<button>开始搜索</button>
            </div>
		</div>
	</div>
</div>
<div class="container">
  <div class="info_cont">
    	<div class="info_tit">
        	<h4>最新拼车信息</h4>
            <p>更多>></p>
        </div>
   	<table width="998" border="0">
    	  <tr style="background:#f5f5f5;border-bottom:1px solid #ccc;">
    	    <td width="80">类型</td>
    	    <td width="90">头像</td>
    	    <td width="90">昵称</td>
    	    <td width="150">出发日期</td>
    	    <td width="90">价位</td>
    	    <td width="80">车型</td>
            <td width="80">出发城市</td>
    	    <td width="80">目的城市</td>
    	    <td width="60">途经</td>
    	    <td width="70">座位数</td>
    	    <td width="">联系方式</td>
  	    </tr>
    	  <tr>
    	    <td>代驾</td>
    	    <td><img src="{{ asset('/frontend/images/dashboard/touxiang.png') }}"></td>
    	    <td>用户20</td>
    	    <td>2015-06-01 09：00</td>
    	    <td>100元/位</td>
    	    <td>大众宝来</td>
            <td>宝鸡</td>
    	    <td>西安</td>
    	    <td>咸阳</td>
    	    <td>4/2</td>
    	    <td>查看详情</td>
  	    </tr>
    	  <tr class="color">
    	    <td>即时</td>
    	    <td><img src="{{ asset('/frontend/images/dashboard/touxiang.png') }}"></td>
    	    <td>用户20</td>
    	    <td>2015-06-01 09：00</td>
    	    <td>100元/位</td>
    	    <td>大众宝来</td>
            <td>宝鸡</td>
    	    <td>西安</td>
    	    <td>咸阳</td>
    	    <td>4/4</td>
    	    <td>查看详情</td>
  	    </tr>
    	  <tr>
    	    <td>长途</td>
    	    <td><img src="{{ asset('/frontend/images/dashboard/touxiang.png') }}"></td>
    	    <td>用户20</td>
    	    <td>2015-06-01 09：00</td>
    	    <td>100元/位</td>
    	    <td>大众宝来</td>
            <td>宝鸡</td>
    	    <td>西安</td>
    	    <td>咸阳</td>
    	    <td>4/1</td>
    	    <td>查看详情</td>
  	    </tr>
    	  <tr  class="color">
    	    <td>上下班;</td>
    	    <td><img src="{{ asset('/frontend/images/dashboard/touxiang.png') }}"></td>
    	    <td>用户20</td>
    	    <td>2015-06-01 09：00</td>
    	    <td>100元/位</td>
    	    <td>大众宝来</td>
            <td>宝鸡</td>
    	    <td>西安</td>
    	    <td>咸阳</td>
    	    <td>4/2</td>
    	    <td>查看详情</td>
  	    </tr>
  	  </table>
      <div class="page">
      	<a>1</a>
        <a>2</a>
        <a>3</a>
        <a>4</a>
        <a>5</a>
        <a>6</a>
        <a>7</a>
        <a>8</a>
        <a>9</a>
        <input type="text" value="11">
        <a>下一页</a>
        <a>70</a>
      </div>
  </div>
  <div class="map_cont">
  	<div class="left_cont">
    	<div class="left_tit">
        	<a class="selected">我是乘客</a>
            <a>我是司机</a>
        </div>
        <div class="input_cont">
			<div>
			<label>所在城市：</label><input class="city" type="text" value="请选择所在城市">
			</div>
			<div>
			<label>出发地址：</label><input type="text" value="请输入起点">
			</div>
			<div>
			<label>目的地址：</label><input type="text" value="请输入终点">
			</div>
			<div>
			<label>开始时间：</label><input type="text" value="2015-06-01 10:00">
			</div>
			<div>
			<label>结束时间：</label><input type="text" value="2015-06-01 10:00">
			</div>
            <div class="input_btn">
            	<button>搜索车辆</button>
            </div>
		</div>
    </div>
    <div class="right_cont">
    	<img src="{{ asset('/frontend/images/dashboard/map.png') }}">
        <div class="right_search">
        <input type="text" value="请输入"><input class="btn" type="button">
        </div>
    </div>
  </div>
  <div class="refer_cont">
  	<div class="cont1">
     	<h4>拼车达人</h4>
     	<img src="{{ asset('/frontend/images/dashboard/person.png') }}">
        <ul>
        	<li>问个问题</li>
            <li>查出行</li>
            <li class="seled">给个赞</li>
            <li>拼车趣事</li>
            <li>求拼车</li>
            <li>私车出租</li>
        </ul>
    </div>
    <div class="cont2">
    	<h4>金牌司机</h4>
        <img src="{{ asset('/frontend/images/dashboard/siji.png') }}">
        <img src="{{ asset('/frontend/images/dashboard/siji.png') }}">
        <img src="{{ asset('/frontend/images/dashboard/siji.png') }}">
        <img src="{{ asset('/frontend/images/dashboard/siji.png') }}">
        <img src="{{ asset('/frontend/images/dashboard/siji.png') }}">
        <img src="{{ asset('/frontend/images/dashboard/siji.png') }}">
    </div>
    <div class="cont3">
    	<h4>最新资讯</h4>
        <p><span>1</span>私人轿车出租</p>
        <p><span>2</span>私人轿车出租</p>
        <p><span>3</span>张江附近需要用车的可以联系我</p>
        <p><span>4</span>张江附近需要用车的可以联系我</p>
        <p><span>5</span>私人轿车出租</p>
        <p><span>6</span>张江附近需要用车的可以联系我</p>
        <p><span>7</span>私人轿车出租</p>
    </div>
  </div>
</div>
<div class="footer">
    <div class="left_text">
        <div class="list-text">
            <dl class="list">
            <dt>新手上路</dt>
            <dd>新手入门</dd>
            <dd>智能范围搜索</dd>
            <dd>拼车线路</dd>
            <dd>充值与提现</dd>
            </dl>
            <dl class="list">
            <dt>我是乘客</dt>
            <dd>乘客指南</dd>
            <dd>费用问题</dd>
            <dd>纠纷问题</dd>
            <dd>账户信息</dd>
            </dl>
            <dl class="list">
            <dt>我是司机</dt>
            <dd>司机指南</dd>
            <dd>费用问题</dd>
            <dd>纠纷问题</dd>
            <dd>提供排名</dd>
            </dl>
            <dl class="list">
            <dt>安全保障</dt>
            <dd>两种认证</dd>
            <dd>互评诚信系统</dd>
            <dd>售后问题</dd>
            </dl>
            <dl class="list">
            <dt>法律咨询</dt>
            <dd>常见问题</dd>
            <dd>投诉建议</dd>
            <dd>隐私政策</dd>
            </dl>
        </div>
    </div>
    <div class="right_text">
        <img class="erweima" src="{{ asset('/frontend/images/dashboard/erweima.png') }}">
        <p>关注我</p>
	</div>
</div>
@endsection
