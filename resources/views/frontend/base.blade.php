<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>@yield("title","拼车云")</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <!-- Bootstrap 3.3.2 -->
        <!-- Date Picker -->
        <!-- Daterange picker -->
        <link href="{{ asset('/frontend/css/dashboard/footer.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/frontend/css/dashboard/header.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('/frontend/css/dashboard/style.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body class="skin-blue">
        <div class="wrapper">
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->
        </div><!-- ./wrapper -->

    </body>
</html>